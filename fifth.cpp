#include <GLFW/glfw3.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

#define PI 3.14159265359

// ############# STRUCT #############

typedef struct { GLdouble x, y, z; } VECTOR3;
typedef struct { GLdouble x, y, z, w; } VECTOR4;
typedef GLdouble MATRIX4[4][4];
typedef struct{ GLint v[4]; } FACE;

// ############# GLOBAL VARIABLES #############

double rotating = 0.0; // rotate matrix alpha
double upup = 0.0; 		 // the eye.y move step
double zoom = 37.0; 	 // starting zoom size

// contains all object's points
std::vector<VECTOR4> pointsOfTorus;
// contains all object's face
std::vector<FACE> faces;

GLdouble winWidth = 800.0f;
GLdouble winHeight = 600.0f;

MATRIX4 view; 			// camera
MATRIX4 Vc; 				// central projection
MATRIX4 Wc; 				// window to viewport
MATRIX4 Wc_Vc_view; // Wc * Vc * view
MATRIX4 Vc_view; 		// Vc * view

MATRIX4 rotateX; // rotate around X axis

MATRIX4 Wtv_Vc; // Wtw * Vc

MATRIX4 scaleMatrix; // scale Matrix

// the cube move matrices
MATRIX4 Move_1;
MATRIX4 Move_2;
MATRIX4 Move_3;
MATRIX4 Move_4;

VECTOR4 transformedCube[8];

MATRIX4 torusMoveUp; // the move matrix of the torus

VECTOR4 homogen_light; // homogen coorinates light
VECTOR3 light; // inhomogen coordinates light
double light_l_r = 0.0; // moving the light on the z axis
double light_fw_bw = 0.0; // moving the light on the x axis

GLdouble alpha = 0.0;
GLdouble deltaAlpha = (3.14f / 180.0) * 3;

int cStep = 36; // the step of the little circles ( PI / 36 )
int pStep = 6; // the step of the points of one little circle ( PI / 6 )

GLdouble center = 6.0;

// camera vectors
VECTOR3 eye;
VECTOR3 up;
VECTOR3 centerVec;

// ################ FUNCTION DECLARATIONS ##############

VECTOR3 initVector3( GLdouble, GLdouble, GLdouble );
VECTOR4 initVector4( GLdouble, GLdouble, GLdouble, GLdouble );
FACE initFace( GLint, GLint, GLint, GLint );
VECTOR3 vecSub( VECTOR3, VECTOR3 );
VECTOR3 convertToInhomogen( VECTOR4 );
VECTOR4 convertToHomogen( VECTOR3 );
GLdouble getVectorLength( VECTOR3 );
VECTOR3 normalizeVector( VECTOR3 );
GLdouble dotProduct( VECTOR3, VECTOR3 );
VECTOR3 crossProduct( VECTOR3, VECTOR3 );
void initIdentityMatrix( MATRIX4 );
void initPersProjMatrix( MATRIX4, GLdouble );
void initRotateMatrixX( MATRIX4, GLdouble );
void initMoveMatrix( MATRIX4, VECTOR3 );
void initScaleMatrix( MATRIX4, double, double, double );
void initWtvMatrix( MATRIX4, double, double, double, double,
														 double, double, double, double );
void initViewMatrix( MATRIX4, VECTOR3, VECTOR3, VECTOR3 );
VECTOR4 mulMatrixVector( MATRIX4, VECTOR4 );
void mulMatrices( MATRIX4, MATRIX4, MATRIX4 );
VECTOR3 getGravity( FACE );
int compareFacesZ( FACE, FACE );
void init();
void initTransformations();
void cube( int*, MATRIX4, MATRIX4, MATRIX4 );
void keyPressed( GLFWwindow*, GLint, GLint, GLint, GLint );

// ############# GLOBAL INITIALIZATIONS ############

VECTOR4 identityCube[8] =
{
	initVector4(0.5f, 0.5f,-0.5f, 1.0f),
	initVector4(-0.5f, 0.5f,-0.5f, 1.0f),
	initVector4(-0.5f,-0.5f,-0.5f, 1.0f),
	initVector4(0.5f,-0.5f,-0.5f, 1.0f),
	initVector4(0.5f, 0.5f, 0.5f, 1.0f),
	initVector4(-0.5f, 0.5f, 0.5f, 1.0f),
	initVector4(-0.5f,-0.5f, 0.5f, 1.0f),
	initVector4(0.5f,-0.5f, 0.5f, 1.0f),
};

FACE cubefaces[6] =
{
	initFace(0, 4, 7, 3),
	initFace(1, 5, 4, 0),
	initFace(4, 5, 6, 7),
	initFace(3, 2, 1, 0),
	initFace(6, 5, 1, 2),
	initFace(7, 6, 2, 3)
};

// ############# TORUS CLASS #############

class Torus
{
private:
	double R; // R is the distance from the center of the tube to the center of the torus,
	double r; // r is the radius of the tube.
public:

	Torus( double R, double r )
	{
		this->R = R; // the radius of the bigger circles
		this->r = r; // the radius of the small circles
	}

	void drawTorus( MATRIX4 T, MATRIX4 rotate, MATRIX4 move, VECTOR3 color, MATRIX4 view )
	{
		glLineWidth(1.5f);

		VECTOR4 ph, pt;
		VECTOR3 pih;

		MATRIX4 rotate_move;
		mulMatrices(move, rotate, rotate_move );

		MATRIX4 view_rotate_move;
		mulMatrices(view, rotate_move, view_rotate_move);
		// view_rotate_move => view * move * rotate

		double pointStep = ((2 * PI) / (PI / pStep)+1); // the number of points on a circle
		double circleStep = ((2 * PI) / (PI / cStep)+1); // the number of little circles

		// collect the torus points
		for( double phi = 0; phi <= 2*PI+0.001; phi += PI/cStep )
		{
			for( double theta = 0; theta <= 2*PI+0.001; theta += PI/pStep )
		  {
				ph = initVector4( (R+r*cos(theta))*cos(phi), r*sin(theta), (R+r*cos(theta))*sin(phi), 1.0 );
				pt = mulMatrixVector(view_rotate_move, ph);
				//  view_rotate_move => view * rotate * move
				// store the point with the view * rotate * move on it
				pointsOfTorus.push_back( pt );
			}
		}

		// the count of torus points ( the next point's index should be hereIAM )
		int hereIAM = pointsOfTorus.size();

		// create torus faces
		int counter = 1;
		// go through every point except the last "ring"'s points
		for( int i = 0; i < pointsOfTorus.size()-pointStep; i++ )
		{
			if( counter == pointStep )
			{
				counter = 1;
				continue;
			}

			// create the face from the appropriate indexes
			faces.push_back( initFace( i, i+1, i+1+pointStep, i+pointStep ) );
			counter++;
		}

		// collect and create cube points and faces
		cube( &hereIAM, Move_1, scaleMatrix, view );
		cube( &hereIAM, Move_2, scaleMatrix, view );
		cube( &hereIAM, Move_3, scaleMatrix, view );
		cube( &hereIAM, Move_4, scaleMatrix, view );

		// sort all shit together
		std::sort( faces.begin(), faces.end(), compareFacesZ );

		VECTOR3 normalV;
		VECTOR3 toCam;

		// go through all faces
		for( int i = 0; i < faces.size(); ++i )
		{
			VECTOR3 edges[2]=
			{
				vecSub(convertToInhomogen(pointsOfTorus[faces[i].v[0]]),
					   convertToInhomogen(pointsOfTorus[faces[i].v[1]])),
				vecSub(convertToInhomogen(pointsOfTorus[faces[i].v[0]]),
					   convertToInhomogen(pointsOfTorus[faces[i].v[2]]))
			};

			// create the normalVector of the face
			normalV = normalizeVector(crossProduct( edges[0], edges[1] ) );

			VECTOR3 average = getGravity( faces[i] );

			toCam = normalizeVector( vecSub( average, initVector3(0.0f, 0.0f, center) ) );

			// ############ COLORING ############

			double angle = dotProduct(normalV, light) / ( getVectorLength(normalV) * getVectorLength(light) );
			angle += 1;
			angle /= 2;
			//
/*
			if( angle >= 0.5 )
			{
				double temp = angle - 0.5;
				double rate = temp / 0.5;
				glColor3f( 1.0, rate, rate );
			}
			else if( angle < 0.5 )
			{
				glColor3f( angle * 2.0, 0.0, 0.0 );
			}
*/
			glColor3f( angle, angle, angle );

			// ########### DRAWING ############

			// if i could see the face
			if( dotProduct( normalV, toCam ) > 0 )
			{
				glBegin(GL_POLYGON);
				for( int j = 0; j < 4; ++j )
				{
					pt = mulMatrixVector( T, pointsOfTorus[faces[i].v[j]] );
					// T => Wtv * Vc
					pih = convertToInhomogen( pt );

					glVertex2f(pih.x, pih.y);
				}
				glEnd();

				// draw fence
				glColor3f( 0.0, 0.0, 0.0 );
				glBegin(GL_LINE_LOOP);
				for( int j = 0; j < 4; ++j )
				{
					pt = mulMatrixVector(T, pointsOfTorus[faces[i].v[j]] );
					// T => Wtv * Vc
					pih = convertToInhomogen( pt );
					glVertex2f(pih.x, pih.y);
				}
				glEnd();
			} // if end
		} // for faces end

		faces.clear();
		pointsOfTorus.clear();
	} // drawTorus end
};

// ############# INITIALIZER AND OPERATION FUNCTIONS #############

VECTOR3 initVector3( GLdouble x, GLdouble y, GLdouble z )
{
	VECTOR3 P;
	P.x = x;
	P.y = y;
	P.z = z;
	return P;
}
VECTOR4 initVector4( GLclampd x, GLclampd y, GLclampd z, GLclampd w )
{
	VECTOR4 P;
	P.x = x;
	P.y = y;
	P.z = z;
	P.w = w;
	return P;
}
FACE initFace( GLint v0, GLint v1, GLint v2, GLint v3 )
{
	FACE f;
	f.v[0] = v0;
	f.v[1] = v1;
	f.v[2] = v2;
	f.v[3] = v3;
	return f;
}
VECTOR3 vecSub( VECTOR3 a, VECTOR3 b )
{
	return initVector3( b.x - a.x, b.y - a.y, b.z - a.z);
}
VECTOR3 convertToInhomogen( VECTOR4 vector )
{
	return initVector3(vector.x / vector.w, vector.y / vector.w, vector.z / vector.w);
}
VECTOR4 convertToHomogen( VECTOR3 vector )
{
	return initVector4(vector.x, vector.y, vector.z, 1.0);
}
GLdouble getVectorLength( VECTOR3 vec )
{
	return sqrt(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2));
}
VECTOR3 normalizeVector( VECTOR3 vector )
{
	GLdouble length = getVectorLength(vector);
	return initVector3(vector.x / length, vector.y / length, vector.z / length);
}
GLdouble dotProduct( VECTOR3 a, VECTOR3 b )
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}
VECTOR3 crossProduct( VECTOR3 a, VECTOR3 b )
{
	return initVector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x );
}
void initIdentityMatrix( MATRIX4 A )
{
	for( int i = 0; i < 4; i++)
		for( int j = 0; j < 4; j++)
			A[i][j] = 0.0;

	for( int i = 0; i < 4; i++)
		A[i][i] = 1.0;
}
void initPersProjMatrix( MATRIX4 A, GLdouble s )
{
	initIdentityMatrix(A);

	A[2][2] = 0.0f;
	A[3][2] = -1.0f / s;
}
void initRotateMatrixX( MATRIX4 A, GLdouble alpha )
{
	initIdentityMatrix(A);

	A[1][1] = cos( (PI / 180) * alpha );
	A[1][2] = -1.0 * sin( (PI / 180) * alpha );

	A[2][1] = sin( (PI / 180) * alpha );
	A[2][2] = cos( (PI / 180) * alpha );
}
void initMoveMatrix( MATRIX4 A, VECTOR3 d )
{
	initIdentityMatrix(A);
	A[0][3] = d.x;
	A[1][3] = d.y;
	A[2][3] = d.z;
}
void initScaleMatrix( MATRIX4 A, double lambda, double mu, double nu )
{
	initIdentityMatrix(A);
	A[0][0] = lambda;
	A[1][1] = mu;
	A[2][2] = nu;
}
void initWtvMatrix( MATRIX4 A, double vleft, double vbottom, double vright, double vtop,
															 double wleft, double wbottom, double wright, double wtop )
{
	initIdentityMatrix(A);

	// 0, 0, 800, 600, -3, -3, 3, 3

	A[0][0] = (vright-vleft)/(wright-wleft);
	A[1][1] = (vtop-vbottom)/(wtop-wbottom);
	A[0][3] = (vleft-wleft)*( (vright-vleft)/(wright-wleft) );
	A[1][3] = (vbottom-wbottom)*( (vtop-vbottom)/(wtop-wbottom) );
}

void initViewMatrix( MATRIX4 A, VECTOR3 eye, VECTOR3 center, VECTOR3 up )
{
  initIdentityMatrix(A);

  VECTOR3 centerMinusEye = initVector3(center.x - eye.x, center.y - eye.y, center.z - eye.z);
  VECTOR3 f = normalizeVector(initVector3( -centerMinusEye.x, -centerMinusEye.y, -centerMinusEye.z));
  VECTOR3 s = normalizeVector( crossProduct(up, f));
  VECTOR3 u = crossProduct(f, s);

  A[0][0] = s.x;
  A[0][1] = s.y;
  A[0][2] = s.z;
  A[0][3] = -dotProduct(s, eye);

  A[1][0] = u.x;
  A[1][1] = u.y;
  A[1][2] = u.z;
  A[1][3] = -dotProduct(u, eye);

  A[2][0] = f.x;
  A[2][1] = f.y;
  A[2][2] = f.z;
  A[2][3] = -dotProduct(f, eye);
}
VECTOR4 mulMatrixVector( MATRIX4 A, VECTOR4 v )
{
	return initVector4(
		A[0][0] * v.x + A[0][1] * v.y + A[0][2] * v.z + A[0][3] * v.w,
		A[1][0] * v.x + A[1][1] * v.y + A[1][2] * v.z + A[1][3] * v.w,
		A[2][0] * v.x + A[2][1] * v.y + A[2][2] * v.z + A[2][3] * v.w,
		A[3][0] * v.x + A[3][1] * v.y + A[3][2] * v.z + A[3][3] * v.w);
}
void mulMatrices( MATRIX4 A, MATRIX4 B, MATRIX4 C )
{
	GLdouble sum;
	for( int i = 0; i < 4; i++)
		for( int j = 0; j < 4; j++) {
			sum = 0;
			for( int k = 0; k < 4; k++)
				sum = sum + A[i][k] * B[k][j];
			C[i][j] = sum;
		}
}
// returns the average of 4 vector
VECTOR3 getGravity( FACE f )
{
	VECTOR4 gravity = initVector4( (pointsOfTorus[f.v[0]].x + pointsOfTorus[f.v[2]].x ) / 2.0,
	 															 (pointsOfTorus[f.v[0]].y + pointsOfTorus[f.v[2]].y ) / 2.0,
															 	 (pointsOfTorus[f.v[0]].z + pointsOfTorus[f.v[2]].z ) / 2.0,
																 (pointsOfTorus[f.v[0]].w + pointsOfTorus[f.v[2]].w ) / 2.0 );

	return convertToInhomogen( gravity );
}

// ############### COMPARE FUNCTION ##############

int compareFacesZ( FACE a, FACE b ) {
	// compare the two gravity
	return getGravity(a).z < getGravity(b).z;
}

void init()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, winWidth, 0.0f, winHeight, 0.0f, 1.0f);

	// gives the camera position ( x axis positive side )
	eye = initVector3( 37.0, 0.0, 0.0 );
	// gives the look of the camera ( origo )
	centerVec = initVector3( 0.0, 0.0, 0.0 );
	// gives that where is the up ( y axis )
	up = initVector3( 0.0, 1.0, 0.0 );

	// the camera's coordinate system
	initViewMatrix(view, eye, centerVec, up);

	initTransformations();
}

void initTransformations()
{
	initMoveMatrix( torusMoveUp, initVector3( 0.0, 9.0, 0.0 ) );
	initScaleMatrix( scaleMatrix, 5, 18, 5 );

	// centralprojection Matrix
	initPersProjMatrix(Vc, center);

	// window to viewport matrix
  double viewLeft = (winWidth - winHeight) / (winHeight / 4);
	// (800 - 600) / (600 / 4)
	// 		200      /    150
	// 					1.3333

	// vleft, vbottom, vright, vtop,
  // wleft, wbottom, wright, wtop
  initWtvMatrix( Wc, viewLeft, 0.0, 600.0, 600.0,
										 -4.0, 	  -4.0, 4.0,   4.0 );

	// matrix multiplies
	mulMatrices(Vc, view, Vc_view);
	mulMatrices(Wc, Vc_view, Wc_Vc_view);
	// Wc_Vc_view = Wc * Vc * view

	mulMatrices( Wc, Vc, Wtv_Vc);

	homogen_light = initVector4( light_fw_bw + eye.x, 1.0 + eye.y, light_l_r + eye.z, 1.0 );
	homogen_light = mulMatrixVector(view, homogen_light);
	light = convertToInhomogen(homogen_light);
}

// ############# FUNCTION DEFINITIONS ############

void cube( int* whereIam, MATRIX4 move, MATRIX4 scale, MATRIX4 view )
{
	VECTOR4 ph, pt;
	VECTOR3 pih;

	MATRIX4 move_scale;
	mulMatrices(move, scale, move_scale);

	MATRIX4 view_move_scale;
	mulMatrices( view, move_scale, view_move_scale);

	// create and collect cube points
	for (int i = 0; i < 8; i++)
	{
		// view * move * scale
		transformedCube[i] = mulMatrixVector(view_move_scale, identityCube[i]);
		pointsOfTorus.push_back(transformedCube[i]);
	}

	// create cube faces
	for( int i = 0; i < 6; ++i )
	{
		faces.push_back(
			initFace( cubefaces[i].v[0]+(*whereIam),
					  cubefaces[i].v[1]+(*whereIam),
					  cubefaces[i].v[2]+(*whereIam),
					  cubefaces[i].v[3]+(*whereIam)
					)
		);
	}

	(*whereIam)+=8;
}

void keyPressed(GLFWwindow * windows, GLint key, GLint scanCode, GLint action, GLint mods) {
	if (action == GLFW_PRESS || GLFW_REPEAT) {
		switch (key) {
		case GLFW_KEY_LEFT:
			alpha += deltaAlpha;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_RIGHT:
			alpha -= deltaAlpha;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_UP:
			upup+=1.0;
			eye.y = upup;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_DOWN:
			upup-=1.0;
			eye.y = upup;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_KP_SUBTRACT:
			zoom += 0.7;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_KP_ADD:
			if( zoom  > 1e-15 )
				zoom -= 0.7;
			eye.x = cos(alpha) * zoom;
			eye.z = sin(alpha) * zoom;
			initViewMatrix(view, eye, centerVec, up);
			initTransformations();
			break;
		case GLFW_KEY_W:
			light_fw_bw += 0.3;
			initTransformations();
			std::cout << "light: " << light_fw_bw << " " << 1.0 << " " << light_l_r << std::endl;
			std::cout << "camera: " << eye.x << " " << eye.y << " " << eye.z << std::endl;
			break;
		case GLFW_KEY_S:
			light_fw_bw -= 0.3;
			initTransformations();
			std::cout << "light: " << light_fw_bw << " " << 1.0 << " " << light_l_r << std::endl;
			std::cout << "camera: " << eye.x << " " << eye.y << " " << eye.z << std::endl;
			break;
		case GLFW_KEY_A:
			light_l_r += 0.3;
			initTransformations();
			std::cout << "light: " << light_fw_bw << " " << 1.0 << " " << light_l_r << std::endl;
			std::cout << "camera: " << eye.x << " " << eye.y << " " << eye.z << std::endl;
			break;
		case GLFW_KEY_D:
			light_l_r -= 0.3;
			initTransformations();
			std::cout << "light: " << light_fw_bw << " " << 1.0 << " " << light_l_r << std::endl;
			std::cout << "camera: " << eye.x << " " << eye.y << " " << eye.z << std::endl;
			break;
		/*case GLFW_KEY_Q:
			light_u_d += 1.0;
			initTransformations();
			std::cout << "light: " << light_l_r << " " << light_u_d << " " << light_fw_bw << std::endl;
			std::cout << "camera: " << eye.x << " " << eye.y << " " << eye.z << std::endl;
			break;
		case GLFW_KEY_E:
			light_u_d -= 1.0;
			initTransformations();
			std::cout << "light: " << light_l_r << " " << light_u_d << " " << light_fw_bw << std::endl;
			std::cout << "camera: " << eye.x << " " << eye.y << " " << eye.z << std::endl;
			break;
			*/
		}
	}

	glfwPollEvents();
}

int main(int argc, char ** argv)
{
	GLFWwindow* window;

	// Initialize the library
	if( !glfwInit() )
		return -1;

	// Create a windowed mode window and its OpenGL context
	window = glfwCreateWindow( winWidth, winHeight, "Fifth Reality", NULL, NULL );
	if( !window )
	{
		glfwTerminate();
		return -1;
	}

	// Make the window's context current
	glfwMakeContextCurrent( window );

	glfwSetKeyCallback( window, keyPressed );

	init();

	Torus onlyOne = Torus(5.0, 1.0);

	// Loop until the user closes the window
	while( !glfwWindowShouldClose( window ) )
	{
		glClear(GL_COLOR_BUFFER_BIT);

		initRotateMatrixX( rotateX, rotating );

		initMoveMatrix( Move_1, initVector3( -9.5, 9.0, -9.5 ) );
		initMoveMatrix( Move_2, initVector3( -9.5, 9.0, 9.5 ) );
		initMoveMatrix( Move_3, initVector3( 9.5, 9.0, -9.5 ) );
		initMoveMatrix( Move_4, initVector3( 9.5, 9.0, 9.5 ) );

		onlyOne.drawTorus( Wtv_Vc, rotateX, torusMoveUp, initVector3(1.0f, 0.0f, 0.0f), view );
		rotating -= 0.5;

		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}
